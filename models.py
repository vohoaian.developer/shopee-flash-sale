from app import db
from sqlalchemy.sql import func


class Session(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    promotion_id = db.Column(db.BigInteger, unique=True)
    name = db.Column(db.String(200), nullable=False)
    description = db.Column(db.Text, nullable=False)
    start_at = db.Column(db.DateTime(timezone=True), nullable=False)
    end_at = db.Column(db.DateTime(timezone=True), nullable=False)
    created_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    updated_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now(),
                           onupdate=func.now())

    def __repr__(self):
        return f"<Session {self.id} - {self.name} [{self.promotion_id}]>"


class Product(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    product_id = db.Column(db.BigInteger, unique=True, nullable=False)
    shop_id = db.Column(db.BigInteger, nullable=False)
    image = db.Column(db.Text, nullable=False)
    price = db.Column(db.DECIMAL(20, 5), nullable=False)
    is_official = db.Column(db.Boolean, nullable=False)
    name = db.Column(db.Text, nullable=False)
    # category_ids = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    updated_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now(),
                           onupdate=func.now())

    def __repr__(self):
        return f"<Product {self.id} - {self.name}>"


class FlashSaleProduct(db.Model):
    __table_args__ = (
        db.UniqueConstraint('product_id', 'promotion_id'),
    )
    id = db.Column(db.BigInteger, primary_key=True)
    product_id = db.Column(db.BigInteger, db.ForeignKey('product.product_id'), nullable=False)
    promotion_id = db.Column(db.BigInteger, db.ForeignKey('session.promotion_id'), nullable=False)
    raw_discount = db.Column(db.SmallInteger, nullable=False)
    price = db.Column(db.DECIMAL(20, 5), nullable=True)
    stock = db.Column(db.Integer, nullable=True)
    sold = db.Column(db.Integer, nullable=True)
    name = db.Column(db.Text, nullable=False)
    hidden_price = db.Column(db.Text, nullable=True)
    flash_category_id = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    updated_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now(),
                           onupdate=func.now())
    product = db.relationship('Product', backref=db.backref('flash_sale_products', lazy=True), lazy='joined')
    session = db.relationship('Session', backref=db.backref('flash_sale_products', lazy=True))

    def __repr__(self):
        return f"<FlashSaleProduct {self.id} - {self.product_id} - {self.name}>"


class FlashCategory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category_id = db.Column(db.Integer, unique=True)
    name = db.Column(db.String(200), nullable=False)
    image = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    updated_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now(),
                           onupdate=func.now())

    def __repr__(self):
        return f"<FlashSaleProduct {self.id} - {self.name}>"

