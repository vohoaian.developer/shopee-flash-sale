from flask import Flask, jsonify, render_template, request
from flask_sqlalchemy import SQLAlchemy
from utils import *
from sqlalchemy import desc
from datetime import timezone, timedelta

app = Flask(__name__)
app.config.update(
    SQLALCHEMY_DATABASE_URI="postgresql://tdipeofj:CDhsjJFzq_CATeTnwxpePTgsx9_o1XAs@satao.db.elephantsql.com/tdipeofj",
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
    # SQLALCHEMY_ECHO=True
)
# Initialize the database connection
db = SQLAlchemy(app)

from models import Session, Product, FlashSaleProduct

# Create DB tables if not exist
db.create_all()

sort_enum = ["DEFAULT", "PRICE_ASC", "PRICE_DESC", "SOLD_ASC", "SOLD_DESC", "ORIGIN_PRICE_ASC", "ORIGIN_PRICE_DESC", "DISCOUNT_ASC", "DISCOUNT_DESC"]


@app.route('/run')
def run():
    index = int(request.args.get('i')) if request.args.get('i') and request.args.get('i').isdigit() else 0
    sessions = get_sessions()
    try:
        # Get the newest session's info
        current_session = sessions[index]
        promo_id = int(request.args.get('promo_id')) if request.args.get('promo_id') and request.args.get(
            'promo_id').isdigit() else get_promotion_id(current_session)
        # Insert all the sessions to the DB
        insert_sessions(sessions)
        # Get the list of products on sale
        products = get_products(promo_id)
        # Get the products' info
        items = get_items(promo_id, map_product_list_to_item_ids(products))
        # Insert the products to the DB
        insert_products(items)

        return f"Session {promo_id} has been fetched successfully! {len(items)} products have been added!"
    except:
        return "The session you are requesting does not exist!"


@app.route('/')
def main():
    sessions = get_sessions()
    index = int(request.args.get('i')) if request.args.get('i') and request.args.get('i').isdigit() else 0
    current_session = sessions[index]
    promo_id = get_promotion_id(current_session)
    # Receive queries
    selected_category = request.args.get('category_id') if request.args.get('category_id') and request.args.get(
        'category_id').isdigit() else None
    selected_category = None if selected_category is None else int(selected_category)
    selected_is_official = request.args.get('is_official') if request.args.get('is_official') and request.args.get(
        'is_official').isdigit() else None
    selected_is_official = None if selected_is_official is None else bool(int(selected_is_official))
    selected_sort_by = request.args.get('sort_by', 'DEFAULT')
    selected_sort_by = selected_sort_by.upper() if selected_sort_by.upper() in sort_enum else "DEFAULT"
    selected_products_per_page = request.args.get('per_page') if request.args.get('per_page') and request.args.get(
        'per_page').isdigit() else 96
    selected_products_per_page = int(selected_products_per_page)
    selected_page = request.args.get('page') if request.args.get('page') and request.args.get('page').isdigit() else 1
    selected_page = int(selected_page)

    # Prepare products to render
    products_query = FlashSaleProduct.query.join(Product) \
        .filter(FlashSaleProduct.promotion_id == promo_id)
    if selected_category:
        products_query = products_query.filter(FlashSaleProduct.flash_category_id == selected_category)
    if selected_is_official is not None:
        products_query = products_query.filter(Product.is_official == selected_is_official)
    if selected_sort_by == "PRICE_ASC":
        products_query = products_query.order_by(FlashSaleProduct.price)
    elif selected_sort_by == "PRICE_DESC":
        products_query = products_query.order_by(desc(FlashSaleProduct.price))
    elif selected_sort_by == "SOLD_ASC":
        products_query = products_query.order_by(FlashSaleProduct.sold)
    elif selected_sort_by == "SOLD_DESC":
        products_query = products_query.order_by(desc(FlashSaleProduct.sold))
    elif selected_sort_by == "ORIGIN_PRICE_ASC":
        products_query = products_query.order_by(Product.price)
    elif selected_sort_by == "ORIGIN_PRICE_DESC":
        products_query = products_query.order_by(desc(Product.price))
    elif selected_sort_by == "DISCOUNT_ASC":
        products_query = products_query.order_by(FlashSaleProduct.raw_discount)
    elif selected_sort_by == "DISCOUNT_DESC":
        products_query = products_query.order_by(desc(FlashSaleProduct.raw_discount))

    products = products_query.offset((selected_page - 1) * selected_products_per_page).limit(
        selected_products_per_page).all()
    # Total products in this sessions
    total_products = products_query.count()
    shopee_total_products = len(get_products(promo_id))
    # Categories
    cats = list(
        map(lambda cat: cat[0], FlashSaleProduct.query.with_entities(FlashSaleProduct.flash_category_id).filter_by(
            promotion_id=promo_id).distinct()))

    return render_template("index.html", products=products, total_products=total_products,
                           shopee_total_products=shopee_total_products,
                           current_session=current_session, cats=cats, sort_options=sort_enum,
                           selected_category=selected_category, selected_sort_by=selected_sort_by,
                           selected_page=selected_page, selected_products_per_page=selected_products_per_page,
                           selected_is_official=selected_is_official,
                           max_page=math.ceil(total_products / selected_products_per_page),
                           index=index, promo_id=promo_id)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)


def insert_sessions(sessions):
    print(f"There are {len(sessions)} SESSIONS will be processed...")
    try:
        for current_session in sessions:
            try:
                session = Session.query.filter_by(promotion_id=current_session["promotion_id"]).first()
                if session:
                    session.name = current_session["name"]
                    session.start_at = current_session["start_at"]
                    session.end_at = current_session["end_at"]
                    session.description = current_session["description"]

                    print(f"Session {session.promotion_id} has already exist! Updated!")
                else:
                    session = Session(promotion_id=current_session["promotion_id"],
                                      name=current_session["name"],
                                      start_at=current_session["start_at"],
                                      end_at=current_session["end_at"],
                                      description=current_session["description"])

                    db.session.add(session)
                    print(f"Session {session.promotion_id} has been added!")
            except:
                print(f"Session {session.promotion_id} ...Error!")
        db.session.commit()
        print(f"SESSIONS...Committed!")
    except:
        print(f"SESSIONS...Error when committing! ROLLBACK...")
        db.session.rollback()

    print(f"Adding SESSIONS...Done!!!")


def insert_products(products):
    print(f"There are {len(products)} PRODUCTS will be processed...")
    try:
        for current_product in products:
            # Product
            try:
                product = Product.query.filter_by(product_id=current_product["item_id"]).first()
                if product:
                    product.image = current_product["image"]
                    product.price = current_product["price_before_discount"]
                    product.name = current_product["name"]

                    print(f"Product {product.product_id} has already exist! Updated!")
                else:
                    product = Product(product_id=current_product["item_id"],
                                      shop_id=current_product["shop_id"],
                                      image=current_product["image"],
                                      price=current_product["price_before_discount"],
                                      is_official=current_product["is_official"] if current_product[
                                          "is_official"] else False,
                                      name=current_product["name"])

                    db.session.add(product)
                    print(f"Product {product.product_id} has been added!")
            except:
                print(f"Product {product.product_id} ...Error!")

            # FlashSaleProduct
            try:
                sale_product = FlashSaleProduct.query.filter_by(product_id=current_product["item_id"],
                                                                promotion_id=current_product["promotion_id"]).first()
                if sale_product:
                    sale_product.raw_discount = current_product["raw_discount"]
                    sale_product.price = current_product["price"]
                    sale_product.stock = current_product["stock"]
                    sale_product.sold = current_product["sold"]
                    sale_product.name = current_product["name"]
                    sale_product.hidden_price = current_product["hidden_price"]
                    sale_product.flash_category_id = current_product["flash_category_id"]

                    print(f"FlashSaleProduct {sale_product.product_id} has already exist! Updated!")
                else:
                    sale_product = FlashSaleProduct(product_id=current_product["item_id"],
                                                    promotion_id=current_product["promotion_id"],
                                                    raw_discount=current_product["raw_discount"],
                                                    price=current_product["price"],
                                                    stock=current_product["stock"],
                                                    sold=current_product["sold"],
                                                    name=current_product["name"],
                                                    hidden_price=current_product["hidden_price"],
                                                    flash_category_id=current_product["flash_category_id"])

                    db.session.add(sale_product)
                    print(f"FlashSaleProduct {sale_product.product_id} has been added!")
            except:
                print(f"FlashSaleProduct {sale_product.product_id}...Error!")

        db.session.commit()
        print(f"PRODUCTS...Committed!")
    except:
        print(f"Adding PRODUCTS...Error when committing! ROLLBACK...")
        db.session.rollback()

    print(f"Adding PRODUCTS...Done!!!")


@app.template_filter()
def pretty_time(dt):
    tz = timezone(timedelta(hours=7))
    return dt.replace(tzinfo=timezone.utc).astimezone(tz).strftime("%H:%M")


@app.template_filter()
def pretty_datetime(dt):
    tz = timezone(timedelta(hours=7))
    return dt.replace(tzinfo=timezone.utc).astimezone(tz).strftime("%d-%m-%Y %H:%M")
