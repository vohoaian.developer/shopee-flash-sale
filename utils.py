import math
import requests
from datetime import datetime


def map_shopee_session_list_to_session_list(session_list):
    return list(
        map(lambda session: dict(name=session["name"],
                                 description=session["description"],
                                 start_at=datetime.utcfromtimestamp(session["start_time"]),
                                 end_at=datetime.utcfromtimestamp(session["end_time"]),
                                 promotion_id=session["promotionid"]), session_list))


def map_shopee_brief_list_to_product_list(brief_list):
    return list(map(lambda product: dict(category_id=product["catid"],
                                         item_id=product["itemid"]), brief_list))


def map_product_list_to_item_ids(products):
    return list(map(lambda product: product["item_id"], products))


def get_sessions():
    r = requests.get("https://shopee.vn/api/v2/flash_sale/get_all_sessions")
    res = r.json()
    sessions = map_shopee_session_list_to_session_list(res["data"]["sessions"])

    return sessions


def get_promotion_id(session):
    return session["promotion_id"]


def get_products(promotion_id):
    r = requests.get(f"https://shopee.vn/api/v2/flash_sale/get_all_itemids?promotionid={promotion_id}")
    res = r.json()
    products = map_shopee_brief_list_to_product_list(res["data"]["item_brief_list"])

    return products


def get_items(promotion_id, item_ids):
    endpoint = "https://shopee.vn/api/v2/flash_sale/flash_sale_batch_get_items"
    total_items = list()
    for i in range(1, math.ceil(len(item_ids) / 50) + 1):
        print(f"Fetching products from {(i - 1) * 50} to {i * 50}...")
        payload = dict(promotionid=promotion_id, itemids=item_ids[(i - 1) * 50:i * 50], limit=50)
        r = requests.post(endpoint, json=payload)
        res = r.json()
        items = map_shopee_item_list_to_item_list(res["data"]["items"])
        total_items.extend(items)

    return total_items


def map_shopee_item_list_to_item_list(item_list):
    return list(map(lambda item: dict(
        item_id=item["itemid"],
        shop_id=item["shopid"],
        promotion_id=item["promotionid"],
        image=item["image"],
        raw_discount=item["raw_discount"],
        price_before_discount=item["price_before_discount"] / 100000,
        price=(item["price"] / 100000) if item["price"] else None,
        start_at=item["start_time"],
        end_at=item["end_time"],
        is_official=item["is_shop_official"],
        stock=item["flash_sale_stock"],
        sold=(item["flash_sale_stock"] - item["stock"]) if item["flash_sale_stock"] else None,
        name=item["name"],
        promo_name=item["promo_name"],
        hidden_price=item["hidden_price_display"].replace(".", ",") if item["hidden_price_display"] else None,
        flash_category_id=item["flash_catid"],
        category_ids=item["cats"]
    ), item_list))


def get_all_flash_categories(products):
    cats = set()
    for product in products:
        cats.add(product["category_id"])

    return cats
